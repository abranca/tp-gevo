##Install coala
install.packages('scrm_1.7.3-1.tar.gz', repos = NULL, type="source")
install.packages('assertthat_0.2.1.tar.gz', repos = NULL, type="source")
install.packages('RcppArmadillo')
install.packages('rehh')
install.packages('coala_0.6.0.tar.gz', repos = NULL, type="source")
##Functions for sliding windows analysis
#Calculate the average thetaW on a ms object
calculate.thetaW<-function(segsites){
    S<-dim(segsites)[2]
    n<-dim(segsites)[1]
    thetaW<-S/sum(1/(1:(n-1)))
    return(thetaW)
}

#Calculate the average thetaPi on a ms object
calculate.thetaPi<-function(segsites){
    n<-dim(segsites)[1]
    SFS<-table(colSums(segsites))
    DiffMoy<-as.numeric(unlist(lapply(as.numeric(names(SFS)),function(x){(x*(n-x))/(n*(n-1)/2)})))
    thetaPi<-sum(DiffMoy*SFS)
    return(thetaPi)
}
#Calculate Tajima's D
calculate.Dt<-function(segsites){
    S<-dim(segsites)[2]
    n<-dim(segsites)[1]
    a1=sum(1/(1:(n-1)))
    a2=sum(1/(1:(n-1))^2)
    b1=(n+1)/(3*(n-1))
    b2=2*(n^2+n+3)/(9*n*(n-1))
    c1=b1-1/a1
    c2=b2-(n+2)/(a1*n)+a2/(a1^2)
    e1=c1/a1
    e2=c2/(a1^2+a2)
    VarDt<-sqrt(e1*S+e2*S*(S-1))
    Dt<-(calculate.thetaPi(segsites)-calculate.thetaW(segsites))/VarDt
    return(Dt)
}
#calculate thetaL
calculate.thetaL<-function(segsites){
    n<-dim(segsites)[1]
    Epsilon<-colSums(segsites)
    thetaL<-table(Epsilon)
    thetaL<-sum(as.numeric(names(thetaL))*thetaL)/(n-1)
    return(thetaL)
}

#calculate Fay Wu H modified by Zeng
calculate.Hfw<-function(segsites){
    S<-dim(segsites)[2]
    n<-dim(segsites)[1]
    thetaL<-calculate.thetaL(segsites)
    thetaPi<-calculate.thetaPi(segsites)
    thetaW<-calculate.thetaW(segsites)
    an=sum(1/(1:(n-1)))
    bn=sum(1/((1:(n-1))^2))
    theta2<-S*(S-1)/(an^2+bn)
    VarTLPi<-thetaW*(n-2)/(6*(n-1))+theta2*(18*n*n*(3*n+2)*sum(1/(1:n)^2)-(88*n*n*n+9*n*n-13*n+6))/(9*n*(n-1)*(n-1))
    return((thetaPi-thetaL)/sqrt(VarTLPi))
}

#R2 for linkage disequilibrium
rsq<-function(segsites){
    if(dim(segsites)[2]>1){
        if(dim(segsites)[2]>70){
            segsites<-segsites[,sample(1:(dim(segsites)[2]),70,replace=F)]
         }
        poss<-combn(1:(dim(segsites)[2]),2)
        Nind<-dim(segsites)[1]
        rsq<-apply(poss,2,function(x){
            fA<-sum(segsites[,x[1]])/Nind;
            fB<-sum(segsites[,x[2]])/Nind;
            Num<-abs(sum(segsites[,x[1]]==0 & segsites[,x[2]]==1)/Nind-(1-fA)*fB);
            Denom<-sqrt(fA*(1-fA)*fB*(1-fB));
            return(Num/Denom)})
        return(mean(rsq))
    }else{
        return(NA)}
}

#EHH
EHH<-function(segsites,snpfocus,max_length=100){
    freq<-sum(segsites[,snpfocus])/(dim(segsites)[1])
    EHH0<-NULL
    EHH1<-NULL
    if((dim(segsites)[2]-snpfocus+1)>max_length){
        Max<-max_length
    }else{
        Max<-dim(segsites)[2]
    }
    if(snpfocus>max_length){
        Min<-snpfocus-max_length
    }else{
        Min<-1
    }
    for (site in rev(Min:(snpfocus-1))){
        if(sum(segsites[,snpfocus]==0)==1){
            EHH0<-c(1,EHH0)
        } else if(sum(segsites[,snpfocus]==0)==0)
        {
            EHH0<-c(0,EHH0)
        }
        else
        {
            Num<-table(apply(segsites[segsites[,snpfocus]==0,site:snpfocus],1,paste,collapse=""))
            EHH0<-c(sum((Num/sum(Num))^2),EHH0)
        }
    }
    for (site in rev(Min:(snpfocus-1))){
        if(sum(segsites[,snpfocus]==1)==1){
            EHH1<-c(1,EHH1)
        } else if(sum(segsites[,snpfocus]==1)==0)
        {
            EHH1<-c(0,EHH1)
        } else
        {
            Num<-table(apply(segsites[segsites[,snpfocus]==1,site:snpfocus],1,paste,collapse=""))
            EHH1<-c(sum((Num/sum(Num))^2),EHH1)
        }

    }
    EHH0<-c(EHH0,1)
    EHH1<-c(EHH1,1)
    for (site in (snpfocus+1):(snpfocus+Max)){
        if(sum(segsites[,snpfocus]==0)==1){
            EHH0<-c(EHH0,1)
        } else if(sum(segsites[,snpfocus]==0)==0)
        {
            EHH0<-c(EHH0,0)
        } else
        {
            Num<-table(apply(segsites[segsites[,snpfocus]==0,snpfocus:site],1,paste,collapse=""))
            EHH0<-c(EHH0,sum((Num/sum(Num))^2))
        }
    }
    for (site in (snpfocus+1):(snpfocus+Max)){
        if(sum(segsites[,snpfocus]==1)==1){
            EHH1<-c(EHH1,1)
        } else if(sum(segsites[,snpfocus]==1)==0)
        {
            EHH1<-c(EHH1,0)
        }
        else
        {
            Num<-table(apply(segsites[segsites[,snpfocus]==1,snpfocus:site],1,paste,collapse=""))
            EHH1<-c(EHH1,sum((Num/sum(Num))^2))
        }
    }
    return(log(sum(EHH1)/sum(EHH0)))
}

#Sliding Window
sliding.windows.analysis<-function(winsize=0.1,segsites,position,statistics=calculate.Dt,step=0.1){
    Nsites<-length(position)
    Stat<-NULL;
    Pos<-NULL
    i=0;
    while(i<Nsites){
        segwin<-segsites[,position>(i/Nsites) & position<=((i+winsize*Nsites)/Nsites)]
        Pos<-c(Pos,mean(c(i/Nsites,((i+winsize*Nsites)/Nsites))))
        Stat<-c(Stat,do.call(statistics,args=(list(segsites=segwin))));
        i=i+step*Nsites
    }
    return(cbind(Pos,Stat))
}

###########################################################################################
###########################################################################################
##Coalescence simulations
#loading msms in coala
list.of.packages <- c("coala","ape")
new.packages <- list.of.packages[!(list.of.packages %in% installed.packages()[,"Package"])]
if(length(new.packages)){
    install.packages(new.packages)
}
library(coala)
activate_msms("/Applications/msms3.2rc-b163.jar",priority = 1000)
#Selection model
Nlines=50 #Number of lineages ie haplotypes
K=10#Number of chromosomes ie independant loci
Theta=10000 #Theta = 4Neu
Rho=1000 #Rho=4ner
model_sel <- coal_model(Nlines,K) +
feat_mutation(Theta,fixed_number=T) +
feat_recombination(Rho) +
feat_selection(strength_A = 10000,time=0.1,start=TRUE,Ne=100)+
sumstat_sfs() +
sumstat_tajimas_d() +
sumstat_nucleotide_div() +
sumstat_seg_sites()+
sumstat_trees()


stats_sel<-simulate(model_sel)


#plot output
par(mfrow=c(3,4))
barplot(stats_sel$sfs,main="Derived SFS")

#sliding windows
#Tajima's Theta
TP<-sapply(stats_sel$seg_sites,function(x){sliding.windows.analysis(segsites=x$snps,position=x$position,winsize=0.05,step=0.025,statistics=calculate.thetaPi)},simplify=T)
if(K>1){
    TP<-cbind(TP[1:(length(TP[,1])/2),1],rowMeans(TP[(length(TP[,1])/2+1):length(TP[,1]),]))
} else {
    TP<-cbind(TP[1:(length(TP[,1])/2)],TP[(length(TP[,1])/2+1):length(TP[,1])])
}
plot(TP,type="l",xlab="position",ylab=bquote(theta[pi]),cex.lab=1.1)

#Watterson's Theta
TW<-sapply(stats_sel$seg_sites,function(x){sliding.windows.analysis(segsites=x$snps,position=x$position,winsize=0.05,step=0.025,statistics=calculate.thetaW)},simplify=T)
if(K>1){
    TW<-cbind(TW[1:(length(TW[,1])/2),1],rowMeans(TW[(length(TW[,1])/2+1):length(TW[,1]),]))
} else {
    TW<-cbind(TW[1:(length(TW[,1])/2)],TW[(length(TW[,1])/2+1):length(TW[,1])])
}
plot(TW,type="l",xlab="position",ylab=bquote(theta[W]),cex.lab=1.1)

#Tajima's D
DT<-sapply(stats_sel$seg_sites,function(x){sliding.windows.analysis(segsites=x$snps,position=x$position,winsize=0.05,step=0.025,statistics=calculate.Dt)},simplify=T)
if(K>1){
    DT<-cbind(DT[1:(length(DT[,1])/2),1],rowMeans(DT[(length(DT[,1])/2+1):length(DT[,1]),]))
} else {
    DT<-cbind(DT[1:(length(DT[,1])/2)],DT[(length(DT[,1])/2+1):length(DT[,1])])
}
plot(DT,type="l",xlab="position",ylab=bquote(D[t]),cex.lab=1.1)

#Fay Wu's H
FW<-sapply(stats_sel$seg_sites,function(x){sliding.windows.analysis(segsites=x$snps,position=x$position,winsize=0.05,step=0.025,statistics=calculate.Hfw)},simplify=T)
if(K>1){
    FW<-cbind(FW[1:(length(FW[,1])/2),1],rowMeans(FW[(length(FW[,1])/2+1):length(FW[,1]),]))
} else {
    FW<-cbind(FW[1:(length(FW[,1])/2)],FW[(length(FW[,1])/2+1):length(FW[,1])])
}
plot(FW,type="l",xlab="position",ylab="Fay and Wu's H",cex.lab=1.1)

#Linkage Disequilibrium Rsquare
RSQ<-sapply(stats_sel$seg_sites,function(x){sliding.windows.analysis(segsites=x$snps,position=x$position,winsize=0.05,step=0.025,statistics=rsq)},simplify=T)
if(K>1){
    RSQ<-cbind(RSQ[1:(length(RSQ[,1])/2),1],rowMeans(RSQ[(length(RSQ[,1])/2+1):length(RSQ[,1]),]))
} else {
    RSQ<-cbind(RSQ[1:(length(RSQ[,1])/2)],RSQ[(length(RSQ[,1])/2+1):length(RSQ[,1])])
}
plot(RSQ,type="l",xlab="position",ylab=bquote(r^2),cex.lab=1.1)

#Zoom 0.4-0.6
plot(TP,type="l",xlab="position",ylab=bquote(theta[pi]),cex.lab=1.1,xlim=c(0.4,0.6))
plot(TW,type="l",xlab="position",ylab=bquote(theta[pi]),cex.lab=1.1,xlim=c(0.4,0.6))
plot(DT,type="l",xlab="position",ylab=bquote(D[t]),cex.lab=1.1,xlim=c(0.4,0.6))
plot(FW,type="l",xlab="position",ylab="Fay and Wu's H",cex.lab=1.1,xlim=c(0.4,0.6))
plot(RSQ,type="l",xlab="position",ylab=bquote(r^2),cex.lab=1.1,xlim=c(0.4,0.6))

#EHH
ust_IHS<-sapply((4*Theta/10):(6*Theta/10),function(x){EHH(stats_sel$seg_sites[[1]]$snps,x,5)})
plot(stats_sel$seg_sites[[1]]$position[(4*Theta/10):(6*Theta/10)],scale(ust_IHS),type="l",ylab="iHS",xlab="position",col="red")
